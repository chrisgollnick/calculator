package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary Divide operation
 */
public class Divide extends AbstractOperation implements BinaryOperation {
float ans = 0;
	@Override
	public float perform(float arg1, float arg2) {
		if (arg2 == 0) {
			return 0;
		} else {
		ans = arg1 / arg2;
	}
		return ans;
	}

	@Override
	public String getName() {
		return "/";
	}

}
